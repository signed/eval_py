# -*- coding: utf-8 -*-
import math
import re
import sys
import tkinter
import typing

import sv_ttk

if sys.version_info[1] < 10:
    raise NotImplementedError("Switch-Case are not implemented on versions < 3.10")

multipliers: typing.Dict[str, float] = {
    "p": 10 ** -12,
    "n": 10 ** -9,
    "u": 10 ** -6,
    "m": 10 ** -3,
    "k": 10 ** 3,
    "M": 10 ** 6
}

options: list = [
    "Carré Symétrique",
    "Carré Asymétrique",
    "Triangulaire Symétrique",
    "Rampe"
]


def roundabout(checked: dict):
    signal = clicked.get()
    match signal:
        case "Carré Symétrique":
            carre_sym(checked)
        case "Triangulaire Symétrique":
            triag_sym(checked)
        case "Rampe":
            rampe(checked)
        case _:
            label_result.config(text="Error")


def carre_sym(checked: dict):  # Signal carré symétrique
    result: float = 4 * checked["UC"] / checked["N"] * math.pi
    label_result.config(text=f"{round(result, 2)} dBV")


def triag_sym(checked: dict):  # Signal triangulaire symétrique
    result: float = (8 * checked["UC"]) / (checked["N"] ** 2 * math.pi ** 2)
    label_result.config(text=f"{round(result, 2)} dBV")


def rampe(checked: dict):  # Signal rampe
    result: float = checked["UC"] / (checked["N"] * math.pi)
    label_result.config(text=f"{round(result, 2)} dBV")


def values():
    label_result.config(text="")  # Effacer le label
    checked: typing.Dict = {"UC": str(entry_U_C.get()), "N": str(entry_N.get())}
    allowed = re.compile(
        r"\d*[umkM]")  # Expression Réguliere des caractères possibles
    for count, variable in enumerate(checked.values()):
        if not allowed.fullmatch(
                variable.replace(".", "")
        ) and variable.replace(".", "").isnumeric(
        ):  # Si la regex ne match pas, mais que c'est que des chiffres (hors point)
            checked[list(checked.keys())[count]] = float(variable)
        elif allowed.fullmatch(variable.replace(
                ".", "")):  # Si la regex match (hors point)
            unit = variable[-1]  # Récuperer l'unité
            checked[list(
                checked.keys())[count]] = float(variable[:-1]) * multipliers[
                unit]  # Multiplier suivant l'unité
        elif not allowed.fullmatch(variable):  # Si la regex ne match pas
            label_result.config(text="Unité non reconnue")
        else:
            print("Erreur inconnue")
    roundabout(checked)


if __name__ == '__main__':
    window = tkinter.Tk()  # Init

    sv_ttk.use_light_theme()
    clicked = tkinter.StringVar()  # Variable tkinter pour sauvegarder l'état du dropdown
    clicked.set("Carré Symétrique")  # Valeur par défaut
    # Labels
    label_U_C = tkinter.Label(window, text="Valeur de l'amplitude crète: ")  # UC
    label_U_C.grid(row=0, column=0, sticky=tkinter.W, pady=2)  # Grille
    label_N = tkinter.Label(window, text="Rang N: ")  # N
    label_N.grid(row=1, column=0, sticky=tkinter.W, pady=2)
    label_result = tkinter.Label(window, text="")  # Résultat
    label_result.grid(row=3, column=1, sticky=tkinter.W, pady=2)

    # Entries
    entry_U_C = tkinter.Entry(window)  # UC
    entry_U_C.grid(row=0, column=1, pady=2)  # Position UC
    entry_N = tkinter.Entry(window)  # N
    entry_N.grid(row=1, column=1, pady=2)  # N
    entry_type = tkinter.OptionMenu(window, clicked, *options)  # Type de signal
    entry_type.grid(row=2, column=1, pady=2)  # Position type de signal

    # Buttons
    action1_button = tkinter.Button(window, text="Calculer FFT", command=values)
    action1_button.grid(row=4, column=0, pady=3)
    quit_button = tkinter.Button(window, text="QUIT", command=window.destroy)
    quit_button.grid(row=4, column=2, pady=3)
    tkinter.mainloop()
