# Evaluation Python 01/07

Utilisation :

Installation de [Pipenv](https://pypi.org/project/pipenv/)
```shell
pip install --user pipenv
```

Clonage de la repo

```shell
git clone https://codeberg.org/signed/eval_py.git
```

Création de l'environnement
```shell
pipenv install
```

Lancement du programme
```shell
pipenv run formichella_ig.py
```
